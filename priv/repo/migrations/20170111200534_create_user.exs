defmodule WikiPhx.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string
      add :password_hash, :string
      add :username, :string,                     null: false
      add :role, :string,                         default: "User", null: false
      add :firstname, :string,                    default: "", null: false
      add :lastname, :string,                     default: "", null: false
      add :deactivated, :boolean,                 default: false, null: false
      add :deactivated_by, :integer
      add :image, :string
      add :provider, :string
      add :uid, :string
      add :reset_password_token, :string
      add :reset_password_sent_at, :utc_datetime
      add :confirmation_token, :string
      add :confirmation_sent_at, :utc_datetime
      add :confirmed_at, :utc_datetime
      add :phonenumber, :string
      add :phonenumber_token, :string
      add :phonenumber_confirmed_at, :utc_datetime
      add :failed_attempts, :integer,             default: 0, null: false
      add :unlock_token, :string
      add :locked_at, :utc_datetime
      add :failed_attempts_stared_at, :utc_datetime

      timestamps()
    end
    create unique_index(:users, [:username])
    create unique_index(:users, [:phonenumber])
    create index(:users, [:password_hash])
    create unique_index(:users, [:provider, :uid], name: :index_provider_uid)

  end
end
