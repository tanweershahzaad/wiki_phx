defmodule WikiPhx.V1.UserControllerTest do
  use WikiPhx.ConnCase
  alias WikiPhx.User
  import WikiPhx.Factory.User

  @invalid_user_attrs %{username: 0, email: "abc@gmail.com", password: "******"}
  @valid_user_attrs %{username: "Jane Smith", email: "abc@gmail.com", password: "******"}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    user = build(:user) |> add_first_last_name |> insert
    conn = get conn, v1_user_path(conn, :index)
    assert json_response(conn, 200)["users"] == [
      %{
        "email" => user.email,
        "firstname" => user.firstname,
        "lastname" => user.lastname,
        "username" =>  user.username
      }
    ]
  end

  test "shows chosen resource", %{conn: conn} do
    user = insert(:user)
    conn = get conn, v1_user_path(conn, :show, user)

    assert json_response(conn, 200)["user"] == %{
      "email" => user.email,
      "username" => user.username,
      "firstname" => user.firstname,
      "lastname" => user.lastname,
    }
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, v1_user_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, v1_user_path(conn, :create), user: @valid_user_attrs
    assert json_response(conn, 201)["user"]["username"]
    assert Repo.get_by(User, %{username: @valid_user_attrs.username})
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, v1_user_path(conn, :create), user: @invalid_user_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    user = insert(:user)
    conn = put conn, v1_user_path(conn, :update, user), user: @valid_user_attrs
    assert json_response(conn, 200)["user"]
    assert Repo.get_by(User, %{username: @valid_user_attrs.username})
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    user = insert(:user)
    conn = put conn, v1_user_path(conn, :update, user), user: @invalid_user_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    user = insert(:user)

    conn = delete conn, v1_user_path(conn, :delete, user)
    assert response(conn, 204)
    refute Repo.get(User, user.id)
  end
end
