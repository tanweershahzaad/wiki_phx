defmodule WikiPhx.UserTest do
  use WikiPhx.ModelCase

  alias WikiPhx.User

  @valid_attrs %{
    confirmation_sent_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010},
    confirmed_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010}, deactivated: true,
    confirmation_token: "some content", deactivated_by: 42, email: "abc@gmail.com", failed_attempts: 42,
    failed_attempts_stared_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010},
    firstname: "some content", lastname: "some content", unlock_token: "some content",
    locked_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010}, username: "some content",
    password_hash: "some content", phonenumber: "some content", provider: "some content",
    reset_password_sent_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010},
    reset_password_token: "some content", role: "some content", uid: "some content"
  }
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
end
