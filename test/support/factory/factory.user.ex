defmodule WikiPhx.Factory.User do
  use ExMachina.Ecto, repo: WikiPhx.Repo

  def user_factory do
    %WikiPhx.User{
      username: "Jane Smith",
      email: sequence(:email, &"email-#{&1}@example.com"),
      role: "User",
      provider: sequence(:provider, &"provider-#{&1}"),
      uid: sequence(:uid, &"uid-#{&1}"),
    }
  end

  def make_admin(user) do
    %{user | role: "Admin"}
  end

  def invalid_username(user) do
    %{user | username: 0}
  end

  def add_first_last_name(user) do
    %{user | firstname: "John", lastname: "Doe"}
  end

  def confirm(user) do
    %{user |
      confirmation_sent_at: %Ecto.DateTime{year: 2016, month: 4, day: 22, hour: 2, min: 23, sec: 0},
      confirmed_at: %Ecto.DateTime{year: 2016, month: 4, day: 22, hour: 2, min: 23, sec: 0}
    }
  end
end
