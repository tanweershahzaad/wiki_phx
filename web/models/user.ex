defmodule WikiPhx.User do
  use WikiPhx.Web, :model

  schema "users" do
    field :email, :string
    field :password_hash, :string
    field :password, :string,                       virtual: true
    field :username, :string,                       null: false
    field :role, :string,                           default: "User",  null: false
    field :firstname, :string,                      default: "",      null: false
    field :lastname, :string,                       default: "",      null: false
    field :deactivated, :boolean,                   default: false
    field :deactivated_by, :integer
    field :image, :string
    field :provider, :string
    field :uid, :string
    field :reset_password_token, :string
    field :reset_password_sent_at, Ecto.DateTime
    field :confirmation_token, :string
    field :confirmation_sent_at, Ecto.DateTime
    field :confirmed_at, Ecto.DateTime
    field :phonenumber, :string
    field :phonenumber_token, :string
    field :phonenumber_confirmed_at, Ecto.DateTime
    field :failed_attempts, :integer,               default: 0,       null: false
    field :unlock_token, :string
    field :locked_at, Ecto.DateTime
    field :failed_attempts_stared_at, Ecto.DateTime

    timestamps()
  end

  @required_fields [:username]
  # Donot put password_hash in fields
  @fields [
    :email,
    :username,
    :firstname,
    :lastname,
    :provider,
    :uid,
    :phonenumber,
    :password
  ]

  # In Phoenix, we create “pure” function in our models and views, and “impure” functions… elsewhere. What is a “pure” function?
  # Think of a function that takes in 2 values e.g fn (a, b) -> a + b end
  # If you call this function with 1 and 2, the result will always be the same. Now imagine calling this code: Repo.all(Account)
  # This code might return 1 User, or 300.

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:username)
    |> unique_constraint(:phonenumber)
    |> unique_constraint(:email)
    |> unique_constraint(:uid, name: :index_provider_uid)
    |> validate_format(:email, ~r/.+@.+/)
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def registration_changeset(struct, params \\ %{}) do
    changeset(struct, params)
    |> validate_required(:password)
    |> hash_password
    |> validate_new_user
  end

  defp validate_new_user(changeset) do
    email = get_field(changeset, :email)
    phonenumber = get_field(changeset, :phonenumber)

    if (!email || email == "") && (!phonenumber || phonenumber == "") do
      changeset
      |> add_error(:email, "email and phone number both can't be blank")
    else
      changeset
    end
  end

  defp hash_password(changeset) do
    password = changeset.changes.password
    changeset
    |> validate_length(:password, min: 6, max: 50)
    |> put_change(:password_hash, Comeonin.Bcrypt.hashpwsalt(password))
  end
end
