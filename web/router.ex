defmodule WikiPhx.Router do
  use WikiPhx.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", WikiPhx do
    pipe_through :api

    scope "/v1", V1, as: :v1 do
      resources "/users", UserController, except: [:new, :edit]
    end
  end
end
