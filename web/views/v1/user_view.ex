defmodule WikiPhx.V1.UserView do
  use WikiPhx.Web, :view

  def render("index.json", %{users: users}) do
    %{users: render_many(users, WikiPhx.V1.UserView, "user.json")}
  end

  def render("show.json", %{user: user}) do
    %{user: render_one(user, WikiPhx.V1.UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    %{
      email: user.email,
      username: user.username,
      firstname: user.firstname,
      lastname: user.lastname
    }
  end
end
